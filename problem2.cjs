const fs = require(`fs`);

const path = require(`path`);

function readFile(fileName) {

    const readPromise = new Promise((resolve, reject) => {

        fs.readFile(fileName, "utf8", function (err, data) {

            if (err) {
                reject("Unable to read file");
            } else {

                resolve(data);
            }
        });

    });

    return readPromise;

}



function Touppercase(data1) {

    let fileName2 = "uppercase_lipsum.txt";

    let data2 = data1.toUpperCase();


    const convertFilePromise = new Promise((resolve, reject) => {

        fs.writeFile(fileName2, data2, (error) => {

            if (error) {
                console.log(error);
            } else {

                fs.writeFile("filenames.txt", "uppercase_lipsum.txt", (error) => {

                    if (error) {
                        reject(error);
                    } else {
                        resolve(data2);

                    }

                })
            }
        })


    });

    return convertFilePromise;

}


function TolowercaseSplit(data2) {

    let fileName3 = "lowercase_Splitted_sentences_lipsum.txt";

    let data3 = data2.toLowerCase()
        .split(". ")
        .join(".\n");

    const lowercasePromise = new Promise((resolve, reject) => {

        fs.writeFile(fileName3, data3, (error) => {

            if (error) {
                reject(error);
            } else {

                fs.appendFile("filenames.txt", "\n" + fileName3, (error) => {

                    if (error) {
                        reject(error);
                    } else {

                        resolve(data3);

                    }

                })

            }
        });
    });

    return lowercasePromise;

}


function SortFile(data3) {

    let sortedFile = "sorted_lipsum.txt";

    const sortFilePromise = new Promise((resolve, reject) => {

        fs.readFile("uppercase_lipsum.txt", "utf8", function (err, data) {

            if (err) {
                reject(err);
            }

            let allData = data + data3;

            allData = allData.split(" ");

            allData.sort();

            fs.writeFile(sortedFile, allData.toString(), (err) => {

                if (err) {
                    reject(err);
                } else {

                    fs.appendFile("filenames.txt", "\n" + sortedFile, (error) => {

                        if (error) {
                            reject(error);
                        } else {

                            resolve(allData);

                        }

                    })

                }
            });


        });

    });

    return sortFilePromise;
}

function deleteNewFiles() {

    const deleteFilesPromise = new Promise((resolve, reject) => {

        fs.readFile("filenames.txt", "utf8", (err, data) => {

            if (err) {
                reject(err);
            }

            let filesArray = data.split("\n");

            filesArray.map(filename => {

                fs.unlink(filename, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve("All files deleted sucessfully!!");
                    }
                })
            });


        });


    });

    return deleteFilesPromise;

}


module.exports.readFunction = readFile;

module.exports.uppercaseFunction = Touppercase;

module.exports.lowercaseSplitFunction = TolowercaseSplit;

module.exports.sortFunction = SortFile;

module.exports.deleteFilesFunction = deleteNewFiles;
