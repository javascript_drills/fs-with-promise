
const fs = require(`fs`);

const path = require(`path`);

function makeDirectory(filepath) {

    const makeDirectoryPromise = new Promise((resolve, reject) => {

        fs.access(filepath, err => {

            if (err) {
                fs.mkdir(filepath, (err) => {

                    if (err) {

                        reject(err);

                    } else {

                        console.log("Directory is not  present");

                        resolve("Directory created for the first time ");

                    }
                })
            } else {

                resolve("Directory is present");

            }
        })


    });


    return makeDirectoryPromise;
}


function createJSONFiles(fileLocation, num = 10) {

    const createFilePromise = new Promise((resolve, reject) => {

        let createdFiles = [];

        let errors = [];

        let executedCount = 0;

        for (let index = 0; index < num; index++) {

            let randomName = new Date().getTime().toString() + Math.random().toFixed(2);

            let fileName = randomName + ".Json"

            fs.writeFile(path.join(fileLocation, fileName), "HelloWorld" + randomName, (err) => {

                if (err) {
                    errors.push(err);
                } else {
                    createdFiles.push(fileName);
                }

                executedCount++;

                if (executedCount === num) {


                    if (createdFiles.length === num) {

                        resolve(createdFiles);

                    } else {

                        reject("All files were not created sucessfully!!");

                    }

                }


            });


        }
    });


    return createFilePromise;
}


function deleteJSONFiles(fileLocation, filesArray) {

    const deleteFilepromise = new Promise((resolve, reject) => {

        let deletedFiles = [];

        let errors = [];

        let executedCount = 0;

        filesArray.forEach(element => {

            fs.unlink(path.join(fileLocation, element), (err) => {

                if (err) {

                    errors.push(err);
                } else {

                    deletedFiles.push(element);

                }

                executedCount++;

                if (executedCount === filesArray.length) {


                    if (deletedFiles.length === filesArray.length) {

                        resolve("All files Deleted Sucessfully!!");
                    } else {

                        reject("All files were not Deleted sucessfully!!");

                    }

                }

            });


        });


    });

    return deleteFilepromise;

};

module.exports.deleteFiles = deleteJSONFiles;

module.exports.createDirectory = makeDirectory;

module.exports.createjsonFiles = createJSONFiles;