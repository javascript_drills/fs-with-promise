
const problem2Function = require(`../problem2.cjs`);

const fileName = "lipsum.txt";

problem2Function.readFunction(fileName)
    .then((data) => {
        console.log("file read complete!");
        return problem2Function.uppercaseFunction(data);
    })
    .then((data) => {
        console.log("Converted to Uppercase");
        console.log(`uppercase_lipsum.txt added to filenames.txt`);
        return problem2Function.lowercaseSplitFunction(data);
    })
    .then((data) => {
        console.log("Converted to lowercase & splitted sucessfully");
        console.log(`lowercase_Splitted_sentences_lipsum added to filenames.txt`);
        return problem2Function.sortFunction(data);
    })
    .then((data) => {
        console.log("Sorted sucessfully");
        console.log(`sorted_lipsum.txt added to filenames.txt`);
        return problem2Function.deleteFilesFunction(data)
    })
    .then((message) => {
        console.log(message);
    })
    .catch((error) => {

        console.log(error.message);
    });