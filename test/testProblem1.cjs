
const problem1 = require(`../problem1.cjs`);

const fileLocation = "../jsonFiles";

const numberOfFiles = 5;

problem1.createDirectory(fileLocation)
    .then((result) => {
        console.log(result);
        problem1.createjsonFiles(fileLocation, numberOfFiles)
            .then((fileNamesArray) => {
                console.log("All files created Sucessfully");
                problem1.deleteFiles(fileLocation, fileNamesArray)
                    .then((message) => {

                        console.log(message);

                    });

            })
    })
    .catch((error) => {
        console.log(error);
    });
