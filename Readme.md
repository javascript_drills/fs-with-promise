# Project on File System in Javascipt & application of promise in File system.

## In this project we have worked on various application of File Systems 

### Following are the various operations in the Drill 


1. Problem 1 : -

* Creating Directory.

* Writing File.

* Deleting File.


2. Problem 2 :- 

* Reading File.

* Converting text in the file to Uppercase and storing name of the file in another file containing File names.

* Converting text in the file to Lowercase, splitting text file to sentences & storing name of the file in another file containing File names.

* Combine content from the new files and apply sort on it, store the output in the new file and append the name of new file in file containing the previous file names.

* Read the file names from the file containing names of all the newly created files & Delete the all these files. 